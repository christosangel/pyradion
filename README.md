# pyradion

**pyradion** is an internet radio TUI client, written in **Python**.

![pyradion.png](screenshots/pyradion.png ){height=450}

It shares more or less the same logic with a previous project in `Bash`:

[https://gitlab.com/christosangel/radion](https://gitlab.com/christosangel/radion)

although with  this Python script some more features are available.

---
## Selecting a Station

Through the first menu, moving up and down with the arrow keys, + hitting `Enter`, the user can use one of their (configurable) favorite stations.

Selecting a tag +`Enter`, the user can from the following menu, with the same procedure, select a station from a specific tag, i.e. Jazz, Folk, etc.

The user, through some keybindings, can also proceed to other options like select arandom station, speed dial a station, edit the station list or program preferences etc.

Specifically, the  user can :

- Select directly  a station from the **Favorites** list, which is configurable. This can be done either by moving up/down with arrow keys + `Enter`, or by hitting `0-9` for one of the first 10 favorite stations.


- Select a **tag** to pick a station from.


![tag.png](screenshots/tag.png){height=350}


- Select a **random station** out of this list of Favorite stations.

- Select to pick a station from **All Stations**.

- **Edit Stations** and their tags, by editing the specific file:

![edit_stations](screenshots/edit_stations.png){width=500}

- **Configure Preferences**, by editing the .conf file:

![config](screenshots/config.png){width=500}

- **Find new Stations**, visiting [https://www.radio-browser.info/](https://www.radio-browser.info/)

- **Quit pyradion**.

|  no |  Keybinding     |  Action                       |
|------|-----------------|-----------------------------------|
|   1  |   ↑,k   / ↓, j|   Move up/down the list                |
|   2  |      Enter      |    Select Station/Tag/Action        |
|    3 |     l, L            |    Select Random Station              |
|   4  |    0 - 9          |    Speed-Dial   a Favorite/Tag Station|
|   5  |   a, A            |   Show All Stations List                  |
|    6 |   e, E            |    Edit Stations  File                         |
|   7  |    p, P           |     Edit Config File                            |
|   8  |     f, F           |     Find new Stations                       |
|   9  |     x, X          |       Go Back to Previous Menu    |
|  10 |     q, Q         |      Quit Pyradion                             |


---
## Finding a New Station

- Selecting the option **Find Stations**, the user is directed to  [https://www.radio-browser.info/](https://www.radio-browser.info/).

- There the user can search and find one or more stations that fit their particular and personal taste, e.g. :

![radiobrowser1.png](screenshots/radiobrowser1.png){width=500}

- Clicking on this station, the user is directed to a page with the station's specific details. From all this data, the user picks the **station's url**:

![radiobrowser2.png](screenshots/radiobrowser2.png){width=500}

The user should use **this url** while he adds a new line with this station's data to `stations.txt`, selecting the **Edit Stations** option.

It goes without saying that the user can add stations from any other sourse, too.

---
## Adding a Station to Pyradion

In order to **add a station to Pyradion**, the user edits the `stations.txt` file, either within pyradion (** Edit Stations**), or through any text editor.

**The format of the line should be the following**:

`station-url  ~Name-of-the-Station~ #Tag1 #Tag2 #Tag3`

for instance:

`https://lyd.nrk.no/nrk_radio_jazz_mp3_h ~NRK-Jazz~ #Jazz #Favorites`

- The `station-url` should go first: `https://lyd.nrk.no/nrk_radio_jazz_mp3_h`
- `Name of the Station` should come next, between **tildes** `~`, and whitespaces substituted by **hyphen** `-` : `~NRK-Jazz~`
- Finally, tags come next, starting with **number sign** `#`. `#Jazz #Favorites`

    There can be as many tags in a line as the user likes .

     The **`#Favorites` tag** adds the station to the **Favorites**.

- Adding empty lines to the file has no repercussions to the functionality. Separating lines to groups is also done only for demonstative purposes. To comment out a line, **add `//` at the beginning**.

## Usage

![play.png](screenshots/play.png){height=250}

Once a station is selected to listen to, and the streaming has started, the user will be presented with stream data, like:

- **Station Name**.

- **Song Title** when available.

- **Stream URL**.

- **Player Position, total duration downloaded and streamed percentage**.

- **Cached stream, in seconds and KB/MB**

- **Volume %**

- **Status (Paused, Muted, RECORDING,** Recording output file when recording)


![paused.png](screenshots/paused.png){height=250}


**The data presented is customisable through the `pyradion.config` file.**


The user can use the following mapped keybindings :

| no  |  Keybinding          | Action                           |
|-----|----------------------|----------------------------------|
|  1  |  **␣(space),  p,  P**| **Pause/Play**  Toggle           |
|  2  |       **9 / 0**      |  **Volume** Decrease/Increase     |
|  3  |     m, M             |       **Mute/Unmute**   Toggle        |
|  4  |          **← / →**       |  **Skip back / forward 10 sec**        |
|  5  |     **↑, k  / ↓, j**         |   **Skip back / forward 60 sec** |
|  6  |      **z, Z /  x, X**          |  **Previous / Next station**       |
|  7  |    **r** , **R**                  |   **RECORD  STREAM**   Toggle   |
|  8  |    q, Q              |  **Quit**                           |




![keybindings.png](screenshots/keybindings.png){height=120}




---

## Recording

**Recording a radio stream** in **pyradion** can be done easily, just by hitting **r** or **R**.

![record.png](screenshots/record.png){height=250}

The user will be notified through the *status line* of the recording taking place, and of the **output file name**.

**Editing the `pyradion.config` file, the user will be able to define output audio file directory, as well as the output audio file format.**

If the title is available by the stream, the file will take its name by it.

If title is `Not Available`, the file will take a date/time for name.

### stream-record, or an external bash script?

Since the stream-record option is broken in mpv ( at least according to the [mpv manual](https://mpv.io/manual/master/#usage), and therefore unreliable, a decision was made to use an external bash script for recording the stream, using `sox`.

---

## `genmon-pyradion.sh` script

`pyradion.py` generates a log file, that contains the title of the song that is currently playing. This temp file is `/tmp/pyradion-title.log`.

`genmon-pyradion.sh` is a script that uses this data and in combination with the [Generic Monitor panel plugin](https://docs.xfce.org/panel-plugins/xfce4-genmon-plugin), displays **scrolling the title of the song currently streaming with `pyradion.py`.**

This script is used with the [xfce](https://xfce.org/) desktop environment, and with a little tinkering could easily work with other DEs, e.g. Cinnamon.

---

## Configuring pyradion (Preferences option)

As mentioned above, editing `pyradion.config` file using any text editor, the user can set the following preferences:


|  no |  Variable      |  Action              | Acceptable Values                                                                            |Default |
|-----|----------------|----------------------|----------------------------------------------------------------------------------------------|--------|
|  1  | `logo_conf`    |     Print logo toggle    |  yes / no|yes|
|   2 |  `cheat_conf`  |  Print keybindings cheatsheet toggle |  yes / no                                                                    |yes     |
|   3 |`url_conf`      |     Print url line toggle            |  yes / no                                                                    |yes     |
| 4   | `cache_conf`   |  Print cache line toggle             |  yes / no                                                                    |yes     |
|  5  | `vol_conf`     |    Print volume line toggle          |  yes / no                                                                    |yes     |
|  6  | `color_conf`   |    Colored output toggle             |  yes / no                                                                    |yes     |
| 7 |`emoji_conf`|Show emojis| yes / no | yes|
|  8  | `cycle_conf`   |    Menu Cycle Scrolling Toggle       |  yes / no                                                                    |no      |
|  9  |     `record_directory`        |  Audio File Record Directory|  Absolute / Relative Path                                              |pyradion-music/|
|10|`record_format`        |  Recording Output Format | ogg, vorbis, flac, mp3, wav                                              |mp3|
|  11 | `text_editor`  | Preferred Text Editor|  Any terminal or graphical text editor(*vim, nano, gedit, xed* etc)                          |nano    |
|  12  |  `pref_browser`| Preferred Web Browser|  Any browser to open [https://www.radio-browser.info/](https://www.radio-browser.info/) with.| xdg-open|




---


## Dependencies


The principal dependency is the [mpv](https://mpv.io/) module.

Other modules necessary are termios, sys , tty, time, datetime, random

```
pip3 install mpv termios sys  tty time datetime random

```

For recording, installing `sox` is necessary:

```
sudo apt install sox libsox-fmt-mp3
```
---
## INSTALL

- Open a terminal window and run:

```
git clone https://gitlab.com/christosangel/pyradion/
```

- Change directory to `pyradion/`, make record-toggle.sh executable:


```
cd pyradion/

chmod +x record-toggle.sh
```

- Run `pyradion.py` with `python3`:

```
python3 pyradion.py
```
`stations.txt` and `pyradion.config` *(or at least links to them)* have to be **in the same directory with  `pyradion.py`** in order for things to work.

![png](png/pyradion.png){width=100}

The *pyradion icon* was created by [Freepik - Flaticon](https://www.flaticon.com/free-icons/radio).

***Enjoy!***

---
**Disclaimer: No responsibility is taken regarding the stations, their content, status or whether they are operational or not. Their presence in the `stations.txt` is exclusively demonstrative, represent nobody's taste, nationality, affiliations or orientation, what is more the user is expected to populate this file with stations of their preference. No resposibility is taken regarding use of this software and copyright laws**.


---
