#! /bin/bash
#	 ╭───────────────╮
#	 │ RECORD-TOGGLE │
#	 ╰───────────────╯
#record-toggle.sh is a scrip written by Christos Angelopoulos, October 2023, under GPL v2

REC_PID=$(pidof rec)
if [[ -n $REC_PID ]]
then kill $REC_PID
else
	rec -c 2 -q -r 44100 /tmp/radion-tmp1.wav
	sox /tmp/radion-tmp1.wav "$1" norm
fi
