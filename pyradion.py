#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ╭───╮╭───╮╭───╮╭───╮╭───╮╭───╮╭───╮╭───╮
# │ P ││ Y ││ R ││ A ││ D ││ I ││ O ││ N │
# ╰───╯╰───╯╰───╯╰───╯╰───╯╰───╯╰───╯╰───╯
#A python script written by Christos Angelopoulos, November 2023, under GPL v2
import termios, sys , tty
import mpv
import time
import datetime
import random
import os
from pathlib import Path

def write_log(string):
 log_path0 = "/tmp/pyradion-title.log"
 log_path1 = Path(log_path0)
 logfile = open(log_path1, 'w')
 logfile.write(string)
 logfile.close()

def load_stations_data():
 stations0 = open("stations.txt")
 stations = stations0.read()
 stations0.close()
 stations_lines=stations.split('\n')
 stations_url = []
 stations_name = []
 TAGS = []
 fav_name = []
 fav_url = []
 x=0
 while x < len(stations_lines):
  stations_lines_strip=stations_lines[x].strip()
  comment_check = stations_lines_strip.startswith("//")
  if stations_lines[x] != "" and comment_check != True:
   line_list = stations_lines[x].split(' ')
   stations_url.append(line_list[0])
   stations_name.append(line_list[1])
   if "#Favorites" in stations_lines[x]:
    fav_url.append(line_list[0])
    fav_name.append(line_list[1])
   #TAGS
#  tags0 = stations_lines[x].split(" ")
   y=0
   while y < len(line_list):
    tag_check = line_list[y].startswith("#")
    if tag_check == True and line_list[y] != "#Favorites":
     TAGS.append(line_list[y])
    y+=1
  x+=1
 TAGS = list(dict.fromkeys(TAGS))
 return stations_name, stations_url, fav_name, fav_url, TAGS, stations_lines

def load_emoji(ji):
 if ji['j_config'] == 'yes':
  ji['jluck']='🍀'
  ji['jstar']='⭐'
  ji['jstat']='📋'
  ji['jpref']='🔧'
  ji['jfind']='🔍'
  ji['jquit']='❌'
  ji['jback']='👈'
 else:
  ji['jluck']='  '
  ji['jstar']='  '
  ji['jstat']='  '
  ji['jpref']='  '
  ji['jfind']='  '
  ji['jquit']='  '
  ji['jback']='  '
 return ji
def load_colors(col):
 if col['col_config'] == 'yes':
  col['B']="\033[1;30m"    #Black  : Grid & Data Color
  col['R']="\033[1;31m"    #Red    : Quit, Mute, Pause & Record color
  col['G']="\033[1;32m"    #Green  : Title Color
  col['Y']="\033[1;33m"    #Yellow : Favorites, Station Color
  col['M']="\033[1;35m"    #Magenta: Index Color
  col['C']="\033[1;36m"    #Cyan   : Tags Color
  col['n']="\x1b[0m"       #Normal : Reset to Normal Color
  col['I']="\033[7m"       # Invert
  col['A']="\033[5m"       #Blink
  col['box_width'] = '50'
 else:
  col['B'] = "\x1b[39m"
  col['R'] = "\x1b[39m"
  col['G'] = "\x1b[39m"
  col['Y'] = "\x1b[39m"
  col['M'] = "\x1b[39m"
  col['C'] = "\x1b[39m"
  col['n'] = "\x1b[0m"
  col['I'] = "\033[7m"         # Invert
  col['box_width'] = '46'
 return col

def load_config(confs):
 configs0 = open("pyradion.config")
 configs = configs0.read()
 configs0.close()
 configs_lines=configs.split('\n')
 for i in confs:
  x=1
  while x < len(configs_lines):
   if configs_lines[x] != ''and "#" not in configs_lines[x]:
    if i in configs_lines[x]:
     confs[i] = configs_lines[x].split(' ')[1]
   x += 1
 return confs

def highl(x0,length):
 selected = []
 x=0
 while x < length+6:
  selected.append(col['n'])
  x += 1
 selected[x0]= col['I']
 return selected

def no_None(x0,x):
 if x0 == None:
  x0 = x
 return x0
def timely(t):
 t = int(t)
 days = t//86400
 hours = (t-(days*86400))//3600
 minutes = (t - ((days*86400) + (hours*3600)))//60
 seconds = t - ((days*86400) + (hours*3600) + (minutes*60))
 tt= datetime.time(hours, minutes, seconds)
 return str(tt)

def title0(player):
 tit1 = no_None(player.metadata.get('icy-title'),'')
 tit2 = no_None(player.metadata.get('title'),'')
 tit3 = no_None(player.metadata.get('TITLE'),'')
 tit4 = no_None(player.metadata.get('artist'),'')
 tit5 = no_None(player.metadata.get('ARTIST'),'')
 if tit4 == "" and tit5 == "":
  pavla = ""
 else:
  pavla = " - "
 tit0 = tit1+tit2+tit3+pavla+tit4+tit5
 tit0 = tit0.replace('/', '_')
 if tit0 == '':
  tit0 = "Not Available"
 return tit0

def hex_getch():
 #as found in https://stackoverflow.com/questions/27750536/python-input-single-character-without-enter
 #edited according to https://stackoverflow.com/questions/43770367/printing-unicode-hex-values-for-strings
 fd = sys.stdin.fileno()
 old_settings = termios.tcgetattr(fd)
 try:
  tty.setraw(fd)
  ch = sys.stdin.read(1)     #This number represents the length
 finally:
  termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
 uni_string = hex(ord(ch))[2:].zfill(4)
 return uni_string

def select_station(TAG_SELECTED, stations_lines):
 tag_selected_name=[]
 tag_selected_url=[]
 tag_selected_len = 0
 x=0
 while x < len(stations_lines):
  line_list = stations_lines[x].split(' ')
  if TAG_SELECTED in stations_lines[x]:
   tag_selected_url.append(line_list[0])
   tag_selected_name.append(line_list[1])
   tag_selected_len += 1
  x += 1

 LOOP2 = True
 cursor2 = 0
 getch1 = ''
 getch2 = ''
 highlight2 = []
 while LOOP2 == True:
  highlight2 = highl(cursor2, tag_selected_len)
  print_menu_2(tag_selected_name,tag_selected_url,tag_selected_len,highlight2,cursor2)
  getch = hex_getch()
  if getch == '005b':
   getch1 = '005b'
  elif getch == '001b':
   getch2 = '001b'
  else:
   if getch1 == "005b" and getch2 == '001b':
    getch = getch1+getch2+getch
    getch1 = ''
    getch2 = ''
  if getch == '005b001b0042':         # ARROW DOWN
   if cursor2 < len(highlight2)-1:
    cursor2 += 1
   elif confs['cycle_conf'] == 'yes':
    cursor2 = 0
  elif getch == '005b001b0041':       # ARROW UP
   if cursor2 > 0:
    cursor2 -= 1
   elif confs['cycle_conf'] == 'yes':
    cursor2 = len(highlight2)-1
  elif getch == '0071' or getch == '0051': # q Q:QUIT
   print("\033c")
   quit()
  elif getch == "0058" or getch == "0078": # X x Go Back
   LOOP2 = False
  elif getch == '006c' or getch == '004c':       #  L l : RANDOM
   ran = (random.randrange(0,tag_selected_len-1))
   mpv_PLAY(tag_selected_name, tag_selected_url, ran)
  elif getch == "0065" or getch == "0045": # e E :EDIT STATIONS
   os.system(confs['text_editor']+' stations.txt')
   LOOP1 = False
   LOOP2 = False
  elif getch == "0050" or getch == "0070": # p P :EDIT PREFERENCES
   os.system(confs['text_editor']+' pyradion.config')
   LOOP1 = False
   LOOP2 = False
  elif getch == "0066" or getch == "0046": # f F :FIND NEW STATIONS
   os.system(confs['pref_browser']+' https://www.radio-browser.info/tags')
  elif int(getch, 16)-48 >= 0 and int(getch, 16)-48 <= 9 and int(getch, 16)-48 < tag_selected_len: #0-9 SMART DIAL
   ind = int(getch, 16)-48
   mpv_PLAY(tag_selected_name, tag_selected_url, ind)
  elif getch == "000d":                 #<ENTER> : ENTER
   if cursor2 >= 0 and cursor2 < tag_selected_len:
    mpv_PLAY(tag_selected_name, tag_selected_url, cursor2)
   if cursor2 == tag_selected_len:   #RANDOM STATION
    ran = (random.randrange(0,tag_selected_len-1))
    mpv_PLAY(tag_selected_name, tag_selected_url, ran)
   if cursor2 == tag_selected_len+1: #Go Back
    LOOP2 = False
   if cursor2 == tag_selected_len+2: #EDIT STATIONS
    os.system(confs['text_editor']+' stations.txt')
    LOOP1 = False
    LOOP2 = False
   if cursor2 == tag_selected_len+3: #EDIT PREFERENCES
    os.system(confs['text_editor']+' pyradion.config')
    LOOP1 = False
    LOOP2 = False
   if cursor2 == tag_selected_len+4: #FIND NEW STATIONS
    os.system(confs['pref_browser']+' "https://www.radio-browser.info/tags"' )
    print("\033c")
   if cursor2 == tag_selected_len+5: #QUIT
    print("\033c")
    quit()

def print_menu_2(sel_name,sel_url,sel_len,highlight2,cursor2):
 print("\033c")
 print('\033[?25l') # hide cursor, reset with print('\033[?25h')
 p_list=[]
 p_list.clear()
 if confs['logo_conf'] == 'yes':
  p_list.append(col['B']+"╭─────────────────╮")
  p_list.append(col['B']+"│ "+col['C']+"P Y "+col['G']+"R A D I O N "+col['B']+"│")
  p_list.append(col['B']+"╰─────────────────╯"+col['n'])
 s=0
 screen_lines = int(str(os.get_terminal_size()).split(' ')[1].strip('lines=')[:-1])
 p_list.append(col['B']+"╭──────────────────────Stations─╮")
 while s < sel_len:
  sel_name_line = sel_name[s].strip('~')
  if s < 10:
   smart_dial= str(s)+' '
  else:
   smart_dial = ' '
  SELLINE=col['B']+"│ "+highlight2[s]+col['Y']+smart_dial+sel_name_line.replace("-", " ")+"                                  "
  p_list.append(SELLINE[:int(col['box_width'])]+col['n']+col['B']+"│")
  s +=1
 p_list.append(col['B']+"├───────────────────────Actions─┤")
 p_list.append(col['B']+"│ "+highlight2[s]+col['M']+"L"+col['G']+" "+emo['jluck']+" I Feel Lucky!            "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight2[s+1]+col['M']+"X"+col['G']+" "+emo['jback']+" Go Back                  "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight2[s+2]+col['M']+"E"+col['G']+" "+emo['jstat']+" Edit Stations            "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight2[s+3]+col['M']+"P"+col['G']+" "+emo['jpref']+" Edit Preferences         "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight2[s+4]+col['M']+"F"+col['G']+" "+emo['jfind']+" Find Stations            "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight2[s+5]+col['M']+"Q"+col['R']+" "+emo['jquit']+" Quit Radion              "+col['n']+col['B']+"│")
 p_list.append(col['B']+"├───────────────────────────────┤")
 p_list.append(col['B']+"│"+col['G']+"↑ ↓ : Move,   <Enter> : Select "+col['B']+"│")
 p_list.append(col['B']+"╰───────────────────────────────╯")
 x=0
 if len(p_list) - cursor2 > screen_lines-1:
  while x < screen_lines:
   p_list.append('')
   x += 1
  y = cursor2
  while y >= 0 and cursor2+screen_lines < len(p_list) and y < cursor2+screen_lines-1:
   print(p_list[y])
   y += 1
 else:
  x = 0
  while x < len(p_list):
   print(p_list[x])
   x += 1

def print_menu_1(fav_name, TAGS, highlight1):
 print("\033c")
 print('\033[?25l') # hide cursor, reset with print('\033[?25h')
 p_list=[]
 p_list.clear()
 if confs['logo_conf'] == 'yes':
  p_list.append(col['B']+"╭─────────────────╮")
  p_list.append(col['B']+"│ "+col['C']+"P Y "+col['G']+"R A D I O N "+col['B']+"│")
  p_list.append(col['B']+"╰─────────────────╯"+col['n'])
 screen_lines = int(str(os.get_terminal_size()).split(' ')[1].strip('lines=')[:-1])
 f=0
 t=0
 p_list.append(col['B']+"╭─────────────────────Favorites─╮")
 while f < len(fav_url):
  fav_name_line = fav_name[f].strip('~')
  if f < 10:
   smart_dial= str(f)+' '
  else:
   smart_dial = ' '
  FAVLINE=col['B']+"│ "+highlight1[f]+col['Y']+smart_dial+fav_name_line.replace("-", " ")+"                                  "
  p_list.append(FAVLINE[:int(col['box_width'])]+col['n']+col['B']+"│")
  f +=1
 p_list.append(col['B']+"├──────────────────────────Tags─┤")
 while t < len(TAGS):
  TAGLINE=col['B']+"│ "+highlight1[t+f]+col['C']+TAGS[t].strip("#")+"                                  "
  p_list.append(TAGLINE[:int(col['box_width'])]+col['n']+col['B']+"│")
  t += 1
 p_list.append(col['B']+"├───────────────────────Actions─┤")
 p_list.append(col['B']+"│ "+highlight1[t+f]+col['M']+"L"+col['G']+" "+emo['jluck']+" I Feel Lucky!            "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight1[t+f+1]+col['M']+"A"+col['G']+" "+emo['jstar']+" All Stations             "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight1[t+f+2]+col['M']+"E"+col['G']+" "+emo['jstat']+" Edit Stations            "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight1[t+f+3]+col['M']+"P"+col['G']+" "+emo['jpref']+" Edit Preferences         "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight1[t+f+4]+col['M']+"F"+col['G']+" "+emo['jfind']+" Find Stations            "+col['n']+col['B']+"│")
 p_list.append(col['B']+"│ "+highlight1[t+f+5]+col['M']+"Q"+col['R']+" "+emo['jquit']+" Quit PyRadion            "+col['n']+col['B']+"│")
 p_list.append(col['B']+"├───────────────────────────────┤")
 p_list.append(col['B']+"│"+col['G']+"↑ ↓ : Move,   <Enter> : Select "+col['B']+"│")
 p_list.append(col['B']+"╰───────────────────────────────╯")
 x=0
 if len(p_list) - cursor1 > screen_lines -1:
  while x < screen_lines:
   p_list.append('')
   x += 1
  y = 0
  while y >= 0 and cursor1+screen_lines < len(p_list) and y < cursor1+screen_lines-1 :
   print(p_list[y])
   y += 1
 else:
  x = 0
  while x < len(p_list):
   print(p_list[x])
   x += 1

def cheatsheet(conf):
 if conf == 'yes':
  print("\r"+col['B']+"╭─────┬──────────╮╭─────┬──────────╮")
  print("\r"+col['B']+"│"+col['M']+" 9 0 "+col['B']+"│"+col['C']+"   ↑↓ Vol "+col['B']+"││"+col['M']+"  ␣  "+col['B']+"│"+col['C']+"    Pause "+col['B']+"│")
  print("\r"+col['B']+"├─────┼──────────┤├─────┼──────────┤")
  print("\r"+col['B']+"│"+col['M']+"  m  "+col['B']+"│"+col['C']+"     Mute "+col['B']+"││"+col['M']+" z x "+col['B']+"│"+col['C']+"Prev/Next"+col['B']+" │")
  print("\r"+col['B']+"├─────┼──────────┤├─────┼──────────┤")
  print("\r"+col['B']+"│"+col['M']+" ← → "+col['B']+"│"+col['C']+" Skip 10\""+col['B']+" ││"+col['M']+"  r  "+col['B']+"│"+col['R']+"   Record "+col['B']+"│")
  print("\r"+col['B']+"├─────┼──────────┤├─────┼──────────┤")
  print("\r"+col['B']+"│"+col['M']+" ↑ ↓ "+col['B']+"│"+col['C']+" Skip 60\""+col['B']+" ││"+col['M']+"  q  "+col['B']+"│"+col['R']+"     Quit "+col['B']+"│")
  print("\r"+col['B']+"╰─────┴──────────╯╰─────┴──────────╯"+col['M'])

def print_logo(conf):
 if conf == 'yes':
  print("\r"+col['B']+"╭─────────────────╮")
  print("\r│ "+col['C']+"P Y "+col['G']+"R A D I O N "+col['B']+"│")
  print("\r╰─────────────────╯"+col['n'])

def mpv_PLAY(name_list, url_list, playl_index):
 player = mpv.MPV()
 player.terminal = True
 player.input_terminal = False
 player.really_quiet = True
 STATION = name_list[playl_index].strip('~')
 STATION = col['M']+str(playl_index)+" "+col['Y']+STATION.replace("-", " ")
 STATION_URL = url_list[playl_index]
 player.play(STATION_URL)
 recording = False
 getch1 = ''
 getch2 = ''

 keybind_loop = True
 while keybind_loop == True:
  @player.property_observer('audio-pts')
  def cache_speed_observer(_name, value):
   if confs['url_conf'] == 'yes':
    url_line = "\n\r"+"URL     : "+STATION_URL
   else:
    url_line = ""
   try:
    log_path0 = "/tmp/pyradion-title.log"
    log_path1 = Path(log_path0)
    tit000 = open(log_path1)
    tit00 = tit000.read()
    tit000.close()
    if keybind_loop == True :
     tit0 = title0(player)
     apts = no_None(player.audio_pts,0)
     dur = no_None(player.duration,0)
     per = no_None(player.percent_pos,0)
     cac = no_None(player.demuxer_cache_duration,0)
     cacmem = no_None(player.demuxer_cache_state.get('fw-bytes'),0)
     stat_line = ""
     if confs['vol_conf'] == 'yes':
      vol_line = "\n\rVolume  : "+str(int(player.volume))+"%"
     else:
      vol_line = ""
 #     dat = player.metadata #debug purposes
     if player.mute == True:
      stat_line = "\n\r"+col['R']+'Muted'
     if player.pause == True:
      stat_line = "\n\r"+col['R']+'Paused'
     if recording == True:
      stat_line = "\n\r"+col['R']+col['A']+col['I']+'RECORDING...\n\r'+col['n']+col['B']+'File    : '+file_nam+col['R']+'\n\rHit again \'r\' to stop recording.'
     if cacmem > 1048576:
      cacmem = str(int(cacmem / 1048576))+'MB'
     else:
      cacmem = str(int(cacmem / 1024))+'KB'
     if confs['cache_conf'] == 'yes':
      cache_line = "\n\rCache   : "+str(int(cac))+"s/"+cacmem
     else:
      cache_line = ""
     print("\033c")
     print_logo(confs['logo_conf'])
     cheatsheet(confs['cheat_conf'])
     print("\r"+col['B']+"Station : "+STATION+"\n\r"+col['B']+"Title   : "+col['G']+tit0+col['B']+url_line+"\n\r"+col['B']+"Position: "+timely(apts)+"/"+timely(dur)+" ("+str(int(per))+"%)"+cache_line+vol_line+stat_line+'\033[?25l')
     time.sleep(0.2)
     if tit00 != tit0:
      tit00 = tit0
      write_log(tit00)
   except:
    print("\033c")
    print_logo(confs['logo_conf'])
    cheatsheet(confs['cheat_conf'])
    print("\r"+col['B']+"Station : "+STATION+col['B']+url_line)
    print(col['B']+"\rWaiting for data..."+'\033[?25l')

  getch = hex_getch()
  if getch == '005b':
   getch1 = '005b'
  elif getch == '001b':
   getch2 = '001b'
  else:
   if getch1 == "005b" and getch2 == '001b':
    getch = getch1+getch2+getch
    getch1 = ''
    getch2 = ''
  if getch == '0071' or getch == '0051': # q Q:QUIT
   keybind_loop = False
   write_log('')
   player.terminate()
   print("\033c")
  elif getch == "006d" or getch == "004d": #m M Mute
   if player.mute == True:
    player.mute =False
   else:
    player.mute = True
  elif getch == "0039":   # 9 Vol down
   if player.volume != 0:
    player.volume -= 1
  elif getch == "0030":     # 0 Vol Up
   if player.volume != 150:
    player.volume += 1
  elif getch == '005b001b0043':  # Right arrow seek +10 sec
   player.seek(10)
  elif getch == '005b001b0044':  # Left arrow seek -10 sec
   player.seek(-10)
  elif getch == '005b001b0041' or getch == '006b' :  # Up arrow k seek +60 sec
   player.seek(60)
  elif getch == '005b001b0042' or getch == '006a':  # Down arrow j seek -60 sec
   player.seek(-60)
  elif getch == '002e' :  # ,key seek +1 sec
   player.seek(1)
  elif getch == '002c':  # . key seek -1 sec
   player.seek(-1)
  elif getch == "0020" or getch == "0050" or getch == "0070":      # Space, p,P pause
   if player.pause == False:
    player.pause = True
   else:
    player.pause = False
  elif getch == "0058" or getch == "0078": # X x Next station
   if playl_index < len(url_list)-1:
    playl_index +=1
   else:
    playl_index = 0
   STATION = name_list[playl_index].strip('~')
   STATION = col['M']+str(playl_index)+" "+col['Y']+STATION.replace("-", " ")
   STATION_URL = url_list[playl_index]
   player.stop()
   write_log('')
   player.play(STATION_URL)
  elif getch == "005a" or getch == "007a": # Z z Previous station
   if playl_index > 0 :
    playl_index -=1
    STATION = name_list[playl_index].strip('~')
   else:
    playl_index = len(url_list)-1
   STATION = name_list[playl_index].strip('~')
   STATION = col['M']+str(playl_index)+" "+col['Y']+STATION.replace("-", " ")
   STATION_URL = url_list[playl_index]
   player.stop()
   write_log('')
   player.play(STATION_URL)
  elif getch == "0052" or getch == "0072": # R r Record
   if recording == False:
    recording = True
    tit0 = title0(player).replace("-", " ")
    if tit0 == "Not Available":
     tit0 = str(datetime.datetime.now())[:19]
    file_nam = confs['record_directory']+tit0+'.'+confs['record_format']
    comm_str='./record-toggle.sh '+'"'+file_nam+'" > /dev/null 2>&1 &'
    os.system(comm_str)
   else:
    recording = False
    os.system('./record-toggle.sh')
#########################################
## load defaults, then config file ######
confs = {'logo_conf':'yes', 'cheat_conf':'yes', 'url_conf':'yes', 'vol_conf':'yes', 'cache_conf':'yes', 'color_conf':'yes', 'emoji_conf':'yes', 'cycle_conf':'no', 'record_directory':'pyradion-music/', 'record_format':'mp3', 'text_editor':'nano', 'pref_browser':'xdg-open'}
load_config(confs)
col = {'B': "\033[1;30m",'R': "\033[1;31m",'G':"\033[1;32m",'Y':"\033[1;33m", 'M':"\033[1;35m",'C':"\033[1;36m",'n':"\x1b[0m",'I':"\033[7m",'box_width':  '50', 'col_config':  'yes' }
load_colors(col)
emo = {'jluck':'🍀', 'jstar':'⭐', 'jstat':'📋', 'jpref':'🔧', 'jfind':'🔍', 'jquit':'❌', 'jback':'👈', 'j_config':'yes'}
load_emoji(emo)
tag_selected_name = []
tag_selected_url =[]
##################################
write_log('')
while True:
 print("\033c")
 LOOP1 = True
 cursor1 = 0
 getch1 = ''
 getch2 = ''
 load_config(confs)
 col['col_config'] = confs['color_conf']
 load_colors(col)
 emo['j_config'] = confs['emoji_conf']
 load_emoji(emo)
 stations_data = load_stations_data()
 stations_name = stations_data[0]
 stations_url = stations_data[1]
 fav_name = stations_data[2]
 fav_url = stations_data[3]
 TAGS = stations_data[4]
 stations_lines = stations_data[5]
 total1 = len(TAGS)+len(fav_url)
 while LOOP1 == True:
  highlight1 = highl(cursor1,total1)
  print_menu_1(fav_name,TAGS,highlight1)
  getch = hex_getch()
  if getch == '005b':
   getch1 = '005b'
  elif getch == '001b':
   getch2 = '001b'
  else:
   if getch1 == "005b" and getch2 == '001b':
    getch = getch1+getch2+getch
    getch1 = ''
    getch2 = ''
  if getch == '005b001b0042' or getch == '006a':         # ARROW DOWN j
   if cursor1 < len(highlight1)-1:
    cursor1 += 1
   elif confs['cycle_conf'] == 'yes':
    cursor1 = 0
  elif getch == '005b001b0041' or getch == '006b':       # ARROW UP k
   if cursor1 > 0:
    cursor1 -= 1
   elif confs['cycle_conf'] == 'yes':
    cursor1 = len(highlight1)-1
  elif getch == '0071' or getch == '0051':      # q Q:QUIT
   print("\033c")
   quit()
  elif getch == "0061" or getch == "0041":      # a A :ALL
   TAG_SELECTED = "#"
   select_station(TAG_SELECTED, stations_lines)
   LOOP1 = False
  elif getch == "0065" or getch == "0045":      # e E :EDIT STATIONS
   os.system(confs['text_editor']+' stations.txt')
   LOOP1 = False
  elif getch == "0050" or getch == "0070":      # p P :EDIT PREFERENCES
   os.system(confs['text_editor']+' pyradion.config')
   LOOP1 = False
  elif getch == "0066" or getch == "0046":      # f F :FIND NEW STATIONS
   os.system(confs['pref_browser']+' https://www.radio-browser.info/tags')
  elif getch == '006c' or getch == '004c':      #  l L : RANDOM
   ran = (random.randrange(0,len(fav_url)-1))
   mpv_PLAY(fav_name, fav_url, ran)
  elif int(getch, 16)-48 >= 0 and int(getch, 16)-48 <= 9 and int(getch, 16)-48 < len(fav_url):               #0-9 SMART DIAL
   ind = int(getch, 16)-48
   mpv_PLAY(fav_name, fav_url, ind)
  elif getch == "000d":                         #<ENTER> : ENTER
   if cursor1 >= 0 and cursor1 < len(fav_url):
    mpv_PLAY(fav_name, fav_url, cursor1)
   if  cursor1>=len(fav_url) and cursor1 < len(fav_url)+len(TAGS):
    TAG_SELECTED = TAGS[cursor1-len(fav_url)-len(TAGS)]
    select_station(TAG_SELECTED, stations_lines)
    LOOP1 = False
   if cursor1 == len(fav_url)+len(TAGS):        #RANDOM STATION
    ran = (random.randrange(0,len(fav_url)-1))
    mpv_PLAY(fav_name, fav_url, ran)
   if cursor1 == len(fav_url)+len(TAGS)+1:      #ALL STATIONS
    TAG_SELECTED = "#"
    select_station(TAG_SELECTED, stations_lines)
    LOOP1 = False
   if cursor1 == len(fav_url)+len(TAGS)+2:      #EDIT STATIONS
    os.system(confs['text_editor']+' stations.txt')
    LOOP1 = False
   if cursor1 == len(fav_url)+len(TAGS)+3:      #EDIT PREFERENCES
    os.system(confs['text_editor']+' pyradion.config')
    LOOP1 = False
   if cursor1 == len(fav_url)+len(TAGS)+4:      #FIND NEW STATIONS
    os.system(confs['pref_browser']+' "https://www.radio-browser.info/tags"' )
    print("\033c")
   if cursor1 == len(fav_url)+len(TAGS)+5:      #QUIT
    print("\033c")
    quit()
