#! /bin/bash

GENMON_WIDTH=50 # This variable defines the size of the Generic Monitor plugin, in characters
TYPE=NORMAL
#TYPE=CONTINUOUS

PYRADION_WINDOW_ID="$(wmctrl -l -p|grep 'PyRadion'|awk '{print $1}')"
if [[ -z "$(ps aux|grep pyradion.py|grep -v 'grep')" ]]
then 
 TITLE=""
else
 TITLE="$(cat /tmp/pyradion-title.log)"
 TITLE="${TITLE//&/⅋}" # escape ampersand
fi
if [[ "${TITLE}" == "" ]]
then
 MESSAGE="󱈜 <b>Pyradion</b> not playing"
 SCROLL=""
# echo "1" > /tmp/genmon-pyradion-index.txt
else
 if [[ "$TITLE" == " - " ]];then TITLE="Title Not Available  ";fi
 MESSAGE="󰐹 <b>Pyradion</b> playing:\n""${TITLE}"
 WIDTH=$(echo $TITLE|wc -m)
 if [[ -z $(cat /tmp/genmon-pyradion-index.txt) ]]
 then
  echo "1" > /tmp/genmon-pyradion-index.txt
 fi
 INDEX="$(cat /tmp/genmon-pyradion-index.txt)"
  SCROLL="> ""$TITLE"
  if [[ $TYPE == "CONTINUOUS" ]]
  then
   SCROLL="$TITLE"" > ""$TITLE"
   while [[ $(echo $SCROLL|wc -m) -lt $((GENMON_WIDTH * 3)) ]]
   do
    SCROLL="$SCROLL"" > ""$TITLE"
   done
  if [[ $INDEX -ge $(($WIDTH+3)) ]];then INDEX=1;fi
  else
   PAD=" "
   while [[ "$(echo "$PAD"|wc -m)" -lt $((GENMON_WIDTH/2)) ]]
   do
   PAD="$PAD"" "
   done
   WIDTH=$(echo "$SCROLL"|wc -m)
   SCROLL="$PAD""$PAD""$SCROLL""$PAD""$PAD""$PAD"
   if [[ $INDEX -ge $(($WIDTH+$GENMON_WIDTH)) ]];then INDEX=1;fi
  fi
  SCROLL="${SCROLL:INDEX:GENMON_WIDTH}"
 ((INDEX++))
 echo $INDEX>/tmp/genmon-pyradion-index.txt
fi

echo -e "<tool>""$MESSAGE""</tool><txtclick>wmctrl -i -r $PYRADION_WINDOW_ID -b toggle,hidden</txtclick><txt><tt>""$SCROLL""</tt></txt>"

 
